
import pandas as pd
import glob
import h5py
import numpy as np

f="../..//DijetsDatasets/user.dguest.361023.hbbTraining.e3668_s3126_r10201_p3990.2019_pub.hbbTag_v1_output.h5/user.dguest.20672938._000003.output.h5"

list2=['MV2c10_discriminant', 'IP2D_pb', 'IP2D_pc', 'IP2D_pu', 'IP3D_pb', 'IP3D_pc', 'IP3D_pu', 'JetFitter_N2Tpair', 'JetFitter_dRFlightDir', 'JetFitter_deltaeta', 'JetFitter_deltaphi', 'JetFitter_energyFraction', 'JetFitter_mass', 'JetFitter_massUncorr', 'JetFitter_nSingleTracks', 'JetFitter_nTracksAtVtx', 'JetFitter_nVTX', 'JetFitter_significance3d', 'SV1_L3d', 'SV1_Lxy', 'SV1_N2Tpair', 'SV1_NGTinSvx', 'SV1_deltaR', 'SV1_dstToMatLay', 'SV1_efracsvx', 'SV1_masssvx', 'SV1_pb', 'SV1_pc', 'SV1_pu', 'SV1_significance3d', 'deta', 'dphi', 'dr', 'eta', 'pt', 'rnnip_pb', 'rnnip_pc', 'rnnip_ptau', 'rnnip_pu']

print(len(list2))
print(type(list2))
#h1=pd.read_hdf(f, "subjet_VRGhostTag_1")[list2]
#h1=pd.read_hdf(f, "subjet_VRGhostTag_1")
#print(h1)

#path="../..//DijetsDatasets/full/user.dguest.361023.hbbTraining.e3668_s3126_r10201_p3990.2019_pub.hbbTag_v1_output.h5"
path="../..//DijetsDatasets/user.dguest.361024.hbbTraining.e3668_s3126_r10201_p3990.2019_pub.hbbTag_v1_output.h5/"

filePaths = glob.glob(path+"/"+"*.h5")

for filePath in filePaths:
    print(filePath.split("/")[-1])
    hf= h5py.File(filePath, "r")
    print("\t\tNEvt: " + str(hf["metadata"]["nEventsProcessed"]))
    print("\t\tNEvt: " + str(np.sum(hf["metadata"]["nEventsProcessed"])))
    hf.close()

    #df = pd.read_hdf(filePath, "fat_jet")
    #print("\t\tRows: " + str(len(df)))
    #print("\t\tCols: " + str(len(df.columns)))
    
    print("find subjet 2")
    hdf=pd.HDFStore(filePath)
    h0 = hdf.get("subjet_VRGhostTag_1")
    print("\t\tRows 0: " + str(len(h0)))
    print("\t\tCols 0: " + str(len(h0.columns)))
    h1 = hdf.get("subjet_VRGhostTag_1")[list2]
    print("\t\tRows 1: " + str(len(h1)))
    print("\t\tCols 1: " + str(len(h1.columns)))
    h2 = hdf.get("subjet_VRGhostTag_2")[list2]
    print(len(h2))


